#!/bin/bash
set -e
cd $(dirname $(realpath $0))
echo "Create builder Image"
docker build -t wserv .
echo "Create builder Container"
docker create --name wserv wserv
echo "Copy Binary from builder Container"
docker cp wserv:/app/wserv ./
echo "Remove the builder Container"
docker container rm wserv
echo "Build Done !"
echo "Remove builder Image by hand if need with following cmd:"
echo "docker image rm wserv"
